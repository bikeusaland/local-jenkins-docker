# Dockernized Jenkins #

### What is this repository for? ###

This repository contains what is needed for the creation of a docker container with Jenkins. The Jenkins will be available on port 80. Having aa dockerized Jenkins will allow for better manageability as well as integration into the environment going forward.

### How do use it ###

1.	docker network create jenkins
2.	docker build -t maark-jenkins .
3.	docker run --name Maark-Jenkins --detach --privileged --network jenkins --env DOCKER_HOST=tcp://docker:2376 --env DOCKER_CERT_PATH=/certs/client --env DOCKER_TLS_VERIFY=1   --publish 80:80 --publish 50000:50000   --volume jenkins-data:/var/jenkins_home   --volume jenkins-docker-certs:/certs/client:ro maark-jenkins
4.	docker ps --filter name=Jenkins
5.	docker exec -it Maark-Jenkins bash
6.	cat /var/lib/jenkins/secrets/initialAdminPassword

### To delete existing local Jenkins containers the following command should run ###
docker ps --filter name=jenkins* -aq | xargs docker stop | xargs docker rm
